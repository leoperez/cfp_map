var mainController = function($scope, centersService, districtsService, $filter){

  $scope.map = { center: { latitude: -34.9249288, longitude: -57.9532231}, zoom: 8 };
  $scope.markers =  [];

  var centers = [];

  var setMarkers = function(){
    angular.forEach($scope.centers, function(value, key) {
      var marker = {
        id: value.id,
        coords: {
          latitude: value.lat,
          longitude: value.lng
        }
      }
      $scope.markers.push(marker);
    });
  }
  $scope.$watch('centers', function(){
    $scope.markers =  [];
    setMarkers();
  })
  $scope.selectedCenter = -1;

  $scope.regions = Array.apply(null, Array(25)).map(function (x, i) {
    return i+1;
  })
  centersService.getAll().then(function(data){
    centers = data.centers;
    $scope.centers = centers;
    setMarkers();
  });
  districtsService.getAll().then(function(data){
    $scope.districts = data.districts;
  });

  $scope.selectDistrict = function(){
    $scope.selectedCenter = -1;
    $scope.regionsInput = 0;
    var selected = parseInt($scope.districts[$scope.districtsInput].codigo);
    $scope.centers = $filter('filter')(centers, {numero_distrito: selected}, true);
  };
  $scope.selectRegion = function(){
    $scope.selectedCenter = -1;
    $scope.districtsInput = 0;
    var selected = parseInt($scope.regionsInput);
    $scope.centers = $filter('filter')(centers, {region: selected}, true);
  };
  $scope.getDistrictName = function(id){
    var nombre = $filter('filter')($scope.districts, {codigo: id}, true)[0].nombre;
    return nombre;
  }
  $scope.selectCenter = function($index){
    $scope.selectedCenter = $index;
  }

}
mainController.$inject = ['$scope', 'centersService', 'districtsService', '$filter'];
angular.module('controllers').controller('mainController', mainController);
