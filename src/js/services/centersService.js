var centersService = function($resource){

  var centers = $resource('http://192.168.2.13/cfpmap/centers/:id'+'.json', {id: null});

  this.getAll = function(){
    return centers.get().$promise;
  }
  this.get = function(id){
    return centers.get({id: id}).$promise;
  }
}
centersService.$inject = ['$resource'];
angular.module('services').service('centersService', centersService);
