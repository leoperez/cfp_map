var districtsService = function($resource){

  var districts = $resource('http://192.168.2.13/cfpmap/districts/:id'+'.json', {id: null});

  this.getAll = function(){
    return districts.get().$promise;
  }
  this.get = function(id){
    return districts.get({id: id}).$promise;
  }
}
districtsService.$inject = ['$resource'];
angular.module('services').service('districtsService', districtsService);
