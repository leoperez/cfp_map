module.exports = function(grunt){

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      dev:{
        src: ['src/js/**/*.js']
      }
    },
    compass: {
      dist:{
        options:{
          sassDir: 'src/scss',
          cssDir: 'src/css',
          environment: 'production'
        }
      }
    },
    uglify:{
      controllers:{
        files:{
          'src/js/controllers.js': ['src/js/controllers/*.js']
        }
      },
      services:{
        files:{
          'src/js/services.js': ['src/js/services/*.js']
        }
      }
    },
    watch:{
      html: {
        files:['index.html'],
        options:{
          livereload: true
        }
      },
      js_controllers:{
        files:['src/js/controllers/*.js'],
        tasks: ['uglify:controllers'],
        options: {
          livereload: true
        }
      },
      js_services:{
        files:['src/js/services/*.js'],
        tasks: ['uglify:services'],
        options: {
          livereload: true
        }
      },
      css:{
        files:['src/scss/**/*.scss'],
        tasks: ['compass'],
        options: {
          livereload: true
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  // task setup
  grunt.registerTask('default', []);
}
